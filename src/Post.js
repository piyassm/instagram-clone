import React, { forwardRef, useEffect, useState } from "react";
import "./Post.css";
import { Avatar } from "@material-ui/core";
import { db } from "./firebase";
import firebase from "firebase";

const Post = forwardRef((props, ref) => {
  const { postId, user, username, caption, imageUrl } = props;

  const [comments, setComments] = useState([]);
  const [comment, setComment] = useState("");

  const getComments = (postId) => {
    const unsubscribe = db
      .collection("posts")
      .doc(postId)
      .collection("comments")
      .orderBy("created", "asc")
      .onSnapshot((snapshot) => {
        setComments(
          snapshot.docs.map((doc) => {
            return { id: doc.id, ...doc.data() };
          })
        );
      });

    return () => {
      unsubscribe();
    };
  };

  useEffect(() => {
    getComments(postId);
  }, [postId]);

  const postComment = (e) => {
    e.preventDefault();
    if (user?.displayName) {
      db.collection("posts").doc(postId).collection("comments").add({
        created: firebase.firestore.FieldValue.serverTimestamp(),
        comment: comment,
        username: user.displayName,
      });
      setComment("");
    }
  };

  return (
    <div className="post" ref={ref}>
      <div className="post__header">
        <Avatar
          className="post__avatar"
          src="https://material-ui.com/static/images/avatar/1.jpg"
        />
        <h3>{username}</h3>
      </div>
      <img src={imageUrl} alt="" className="post__image" />
      <h4 className="post__text">
        <strong>{username}: </strong>
        {caption}
      </h4>
      <div className="post__comments">
        {comments.map((comment) => {
          return (
            <p>
              <b>{comment.username}</b> {comment.comment}
            </p>
          );
        })}
      </div>
      {user && (
        <form onSubmit={postComment} className="post__commentBox">
          <input
            className="post__input"
            type="text"
            placeholder="Add a comment..."
            value={comment}
            onChange={(e) => setComment(e.target.value)}
          />
          <button
            disabled={!!!comment}
            className=" "
            type="submit"
            onClick={postComment}
          >
            Post
          </button>
        </form>
      )}
    </div>
  );
});

export default Post;
