import React, { useRef, useState } from "react";
import "./ImageUpload.css";
import { Button } from "@material-ui/core";
import { db, storage } from "./firebase";
import firebase from "firebase";

const ImageUpload = (props) => {
  const { username } = props;
  const [caption, setCaption] = useState("");
  const [image, setImage] = useState(null);
  const [progress, setProgress] = useState(0);

  const fileInput = useRef(null);

  const handleChange = (e) => {
    if (e.target.files[0]) {
      setImage(e.target.files[0]);
    }
  };

  const handleUpload = () => {
    if (!!!username) return alert("username empty");
    if (!!!caption) return alert("caption empty");
    if (!!!image) return alert("image empty");

    const uploadTask = storage
      .ref(`images/${username}/${image.name}`)
      .put(image);
    uploadTask.on(
      "state_changed",
      (snapshot) => {
        const progress = Math.round(
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100
        );
        setProgress(progress);
      },
      (error) => {
        alert(error.message);
      },
      () => {
        storage
          .ref(`images/${username}`)
          .child(image.name)
          .getDownloadURL()
          .then((url) => {
            db.collection("posts").add({
              created: firebase.firestore.FieldValue.serverTimestamp(),
              caption: caption,
              imageUrl: url,
              username: username,
            });
          });
        window.scrollTo(0, 0);
        setProgress(0);
        setCaption("");
        setImage(null);
        fileInput.current.value = "";
      }
    );
  };

  return (
    <div className="imageupload">
      <progress className="imageupload__progress" value={progress} max={100} />
      <input
        type="text"
        placeholder="Enter a caption..."
        onChange={(e) => setCaption(e.target.value)}
        value={caption}
        className="imageupload__input"
      />
      <input type="file" ref={fileInput} onChange={handleChange} />
      <Button className="imageupload__button" onClick={handleUpload}>
        Upload
      </Button>
    </div>
  );
};

export default ImageUpload;
