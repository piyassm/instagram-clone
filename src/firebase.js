import firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyBx-r4-jfLe5ryaSNUU6bK0A8o0wRtLwSQ",
  authDomain: "instagram-clone-30965.firebaseapp.com",
  databaseURL: "https://instagram-clone-30965.firebaseio.com",
  projectId: "instagram-clone-30965",
  storageBucket: "instagram-clone-30965.appspot.com",
  messagingSenderId: "72784158724",
  appId: "1:72784158724:web:e58812a64e22e5575b5551",
  measurementId: "G-QCFR1GLFY8",
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

const db = firebaseApp.firestore();
const auth = firebase.auth();
const storage = firebase.storage();

export { db, auth, storage };
